betterrev
=========

A friendly, hosted wrapper around webrevs

Getting started for developers
------------------------------

* [Download and install play 2.1.1](http://www.playframework.com/documentation/2.1.1/Installing)    
**WARNING**, download Play version **2.1.1** for now, we intend to upgrade at a later stage.

* Install [Git](http://git-scm.com/downloads) for your operating system. 

* Install [Mercurial](http://mercurial.selenic.com/) for you operating system.

* [Fork the betterrev repository](https://bitbucket.org/adoptopenjdk/betterrev/fork)    
**WARNING** Untick the **issues** and **wiki** checkboxes, you do not want those!

* Clone your fork onto your local file system:

`git clone https://<your username>@bitbucket.org/adoptopenjdk/betterrev.git betterrev_project`

* Clone the adopt repository into the betterrev_project/betterrev directory:

```
cd betterrev_project/betterrev 
hg clone https://bitbucket.org/adoptopenjdk/adopt adopt
```

* From within the betterrev_project/betterrev directory, clone the jdk8 repository: 

```
cd adopt
hg clone https://bitbucket.org/adoptopenjdk/jdk8 jdk8
```

* Your final directory structure should look something like this (the important dirs are **betterrev**, **adopt**, **jdk8**):

```
betterrev_project\
    readme.md
    LICENSE
    betterrev\
        adopt\
            jdk8\
                
        app\
        bin\
        conf\
        logs\
        project\
        public\
        target\
        test\
```

* Go to the betterrev_project/betterrev directory and type:

`play run`

* Launch [Betterrev](http://localhost:9000/)

Developing using IntelliJ IDEA 12
---------------------------------

The following is a quick start guide on how to develop with and compile the betterrev project using IntelliJ IDEA 12.
I have tested these instructions using the Ultimate Edition, and I believe they should work on the Community Version,
but a different version of the Scala plugin may be required (if anyone attempts this could they please update the wiki!)

- Download and install the latest version of the [IDEA Scala Plugin](http://plugins.jetbrains.com/plugin/?idea&id=1347 "IDEA Scala Plugin")

- Download install the the latest version of the [IDEA Play 2 Plugin](http://plugins.jetbrains.com/plugin?pr=idea&pluginId=7080 "IDEA Play 2 Plugin")

- Open a Command Line/Terminal and cd to the betterrev directory in the root of the project e.g. <betterrev_project_root>/betterrev/
- Automagically create IDEA project files (this removes the need for you to create your own IDEA project and import the original sources)
```
$ play idea
```
- Compile the project (to prevent compile warnings from being displayed in IDEA)
```
$ play compile
```
It's worth noting at this point that you should ensure that JDK path on the command line is the same as the IDEA JDK. I received the classic
'Generic Major/Minor warning' message when attempting to start the app via IDEA as I had compiled the app using the 1.8-EA JDK on the CLI path, and the
default JDK in IDEA was set to 1.7_09

- Start IDEA and open the project via File -> Open
- Start coding! :-)

Developing using Eclipse Juno
-----------------------------

The following is a quick start guide on how to develop with and compile the betterrev project using Eclipse Juno.

Download and install the latest version of the Eclipse Scala IDE Plugin from the Eclipse marketplace

Open a Command Line/Terminal and cd to the betterrev directory in the root of the project e.g. <betterrev_project_root>/betterrev/
Automagically create Eclipse project files (this removes the need for you to create your own Eclipse project and import the original sources)
```
$ play eclipse
```
Compile the project (to prevent compile warnings from being displayed in Eclipse)
```
$ play compile
```
Start Eclipse and import the project via File -> Import --> General --> Existing Projects into Workspace

Afterwards you'll still have compiler errors, change to using a JDK 1.7.0_10+ JVM and set the source level to 1.7

- Start coding! :-)
