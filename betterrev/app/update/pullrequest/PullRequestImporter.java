package update.pullrequest;

import models.*;

import org.codehaus.jackson.JsonNode;
import org.joda.time.DateTime;

import play.Logger;
import update.BetterrevActor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This Class is responsible for importing BitBucket pullrequests into the Betterrev application.
 */
public final class PullRequestImporter extends BetterrevActor {

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ImportPullRequestsEvent) {
            Logger.debug("ImportPullRequestsEvent received.");

            ImportPullRequestsEvent importRequest = (ImportPullRequestsEvent) message;
            List<PullReview> pullReviews = importAllReviews(importRequest.getJsonNode(), importRequest.getProject());
            eventStream().publish(new PullRequestsImportedEvent(pullReviews));
        }
    }

    public List<PullReview> importAllReviews(JsonNode response, String repositoryId) {
        JsonNode values = response.get("values");
        Iterator<JsonNode> elements = values.getElements();
        List<PullReview> reviews = new ArrayList<>();
        while (elements.hasNext()) {
            PullReview pullReview = importReview(elements.next(), repositoryId);
            reviews.add(pullReview);
        }
        return reviews;
    }

    private PullReview importReview(JsonNode requestNode, String repositoryId) {
        String requestId = requestNode.get("id").asText();
        PullReview review = PullReview.findByBitbucketIds(repositoryId, requestId);
        DateTime updatedOn = getDateTime(requestNode, "updated_on");
        if (review == null) {
            return createPullReview(requestNode, repositoryId, requestId, updatedOn);
        } else {
            ensurePullReviewUpdated(requestNode, updatedOn, review);
            return review;
        }
    }

    private PullReview ensurePullReviewUpdated(JsonNode requestNode, DateTime updatedOn, PullReview review) {
        if (!review.wasUpdatedBefore(updatedOn)) {
            return review;
        }

        review.updatedOn = updatedOn;
        review.name = getTitle(requestNode);
        review.description = getDescription(requestNode);

        String linkToExternalInfo = getLinkToExternalInfo(requestNode);
        return updatePullReview(review, linkToExternalInfo);
    }

    private PullReview updatePullReview(PullReview pullReview, String linkToExternalInfo) {
        if (pullReview.state != State.OPEN) {
            throw new IllegalStateException("Cannot transition from current State of " + pullReview.state);
        }

        pullReview.pullReviewEvents.add(
                new PullReviewEvent(PullReviewEventType.PULL_REVIEW_MODIFIED, linkToExternalInfo));
        pullReview.state = State.OPEN;
        pullReview.update();

        return pullReview;
    }

    private PullReview createPullReview(JsonNode requestNode, String repositoryId, String requestId, DateTime updatedOn) {
        JsonNode userNode = requestNode.get("author");
        String bitbucketUserName = userNode.get("username").asText();
        String displayName = userNode.get("display_name").asText();
        User user = User.findOrCreate(bitbucketUserName, displayName);

        DateTime createdOn = getDateTime(requestNode, "created_on");
        String branchName = getBranchNameFrom(requestNode);
        PullReview review = new PullReview(repositoryId,
                                           requestId,
                                           getTitle(requestNode),
                                           getDescription(requestNode),
                                           user,
                                           createdOn,
                                           updatedOn,
                                           branchName);
        review.save();

        publishPullReviewGeneratedEvent(review, "");

        return review;
    }

    private String getBranchNameFrom(JsonNode requestNode) {
        return requestNode.get("destination")
                          .get("branch")
                          .get("name")
                          .asText();
    }

    private void publishPullReviewGeneratedEvent(PullReview pullReview, String linkToExternalInfo) {
        if (pullReview.state != State.NULL) {
            throw new IllegalStateException("Cannot transition from current State of " + pullReview.state);
        }

        PullReviewEvent pullReviewGeneratedEvent = new PullReviewEvent(
                PullReviewEventType.PULL_REVIEW_GENERATED, linkToExternalInfo
        );

        pullReview.pullReviewEvents.add(pullReviewGeneratedEvent);

        pullReview.state = State.OPEN;
        pullReview.update();

        eventStream().publish(pullReviewGeneratedEvent);
    }

    private static DateTime getDateTime(JsonNode requestNode, String field) {
        String dateString = requestNode.get(field)
                .asText()
                .replace(' ', 'T');
        return DateTime.parse(dateString);
    }

    private static String getDescription(JsonNode requestNode) {
        return requestNode.get("description").asText();
    }

    private static String getTitle(JsonNode requestNode) {
        return requestNode.get("title").asText();
    }

    private String getLinkToExternalInfo(JsonNode requestNode) {
        return requestNode.get("links")
                .get("self")
                .get("href")
                .asText();
    }
}