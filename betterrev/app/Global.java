import akka.actor.ActorRef;
import akka.actor.Props;
import com.gs.collections.api.block.procedure.Procedure2;
import com.gs.collections.impl.map.mutable.UnifiedMap;
import models.PullReviewEvent;
import play.Application;
import play.utils.crud.GlobalCRUDSettings;
import play.libs.Akka;
import update.BetterrevActor;
import update.bitbucket.BitbucketPoller;
import update.bitbucket.PollBitbucketEvent;
import update.mercurial.WebrevGenerator;
import update.pullrequest.ImportPullRequestsEvent;
import update.pullrequest.PullRequestImporter;

import static com.gs.collections.impl.utility.MapIterate.forEachKeyValue;

/**
 * GlobalCRUDSettings extends Play GlobalSettings
 */
public class Global extends GlobalCRUDSettings {

    private final static UnifiedMap<Class<? extends BetterrevActor>, Class<?>> actorToEventMap =
            UnifiedMap.newWithKeysValues(
                    BitbucketPoller.class, PollBitbucketEvent.class,
                    PullRequestImporter.class, ImportPullRequestsEvent.class,
                    WebrevGenerator.class, PullReviewEvent.class
            );

    @Override
    public void onStart(Application app) {
        super.onStart(app);
        if (!app.isTest()) {
            subscribeActorsToEvents();
            pollBitbucket();
        }
    }

    private void subscribeActorsToEvents() {
        forEachKeyValue(actorToEventMap, new Procedure2<Class<? extends BetterrevActor>, Class<?>>() {
            @Override
            public void value(Class<? extends BetterrevActor> actorClass, Class<?> eventClass) {
                ActorRef actor = actorOf(actorClass);
                subscribeActorToEvent(actor, eventClass);
            }
        });
    }

    private ActorRef actorOf(Class<? extends BetterrevActor> actorClass) {
        return Akka.system().actorOf(new Props(actorClass));
    }

    private boolean subscribeActorToEvent(ActorRef actor, Class<?> eventClass) {
        return Akka.system().eventStream().subscribe(actor, eventClass);
    }

    private void pollBitbucket() {
        Akka.system().eventStream().publish(new PollBitbucketEvent());
    }
}
