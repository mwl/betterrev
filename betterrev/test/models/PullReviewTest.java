package models;

import org.joda.time.DateTime;
import org.junit.Test;

import java.util.Collection;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * PullReview entity basic persistence tests.
 */
public class PullReviewTest extends AbstractPersistenceIntegrationTest {

    private static final String ANOTHER_TEST_REPOSITORY_ID = "better-test-repo";
    private static final String TEST_REPOSITORY_ID = "123";
    private static final String TEST_PULL_REQUEST_ID = "123";
    private static final String TEST_PULL_REVIEW_NAME = "Major Mods!";
    private static final String TEST_PULL_REVIEW_DESCRIPTION = "Some change to the code";
    private static final DateTime TEST_PULL_REVIEW_CREATED_ON = DateTime.now();
    private static final DateTime TEST_PULL_REVIEW_UPDATED_ON = DateTime.now();
    private static final int ZERO = 0;
    private static final String TEST_BRANCH_NAME = "sigh";

    public static PullReview createTestInstance() {
        PullReview pullReview = new PullReview(TEST_REPOSITORY_ID, TEST_PULL_REQUEST_ID,
                TEST_PULL_REVIEW_NAME, TEST_PULL_REVIEW_DESCRIPTION, UserTest.createTestInstance(),
                TEST_PULL_REVIEW_CREATED_ON, TEST_PULL_REVIEW_UPDATED_ON, TEST_BRANCH_NAME);

        pullReview.tags.add(TagTest.createTestInstance());
        pullReview.pullReviewEvents.add(new PullReviewEvent(PullReviewEventType.PULL_REVIEW_GENERATED));

        return pullReview;
    }

    public static PullReview createTestInstance(State state) {
        PullReview pullReview = PullReviewTest.createTestInstance();
        pullReview.state = state;
        pullReview.save();

        return pullReview;
    }

    @Test
    public void validPullReviewPersistsCorrectly() {
        PullReview pullReview = createTestInstance();
        assertThat(pullReview.id, is(nullValue()));
        pullReview.save();

        assertThat(pullReview.id, is(not(nullValue())));
    }

    @Test
    public void validPullReviewCreatedOnSetCorrectly() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        assertThat(pullReview.createdOn, is(notNullValue()));
    }

    @Test
    public void validPullReviewCreatedWithGeneratedEvent() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        PullReviewEvent firstPullReviewEvent = pullReview.pullReviewEvents.iterator().next();
        assertThat(firstPullReviewEvent.pullReviewEventType, is(PullReviewEventType.PULL_REVIEW_GENERATED));
    }

    @Test
    public void shouldReturnOnePullReviewEventForAGivenType() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        Collection<PullReviewEvent> pullReviewEvents = pullReview.getPullReviewEventWithType(PullReviewEventType.PULL_REVIEW_GENERATED);
        assertThat(pullReviewEvents.size(), is(1));

        PullReviewEvent firstPullReviewEvent = pullReview.pullReviewEvents.iterator().next();
        assertThat(firstPullReviewEvent.pullReviewEventType, is(PullReviewEventType.PULL_REVIEW_GENERATED));
    }

    @Test
    public void validPullReviewCreatedWithStateNull() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        assertThat(pullReview.state, is(State.NULL));
    }

    /**
     * The unit test below is using the Given..When..Then style,
     * and hence each step (block of code) is separated by a blank line.
     *
     */
    @Test
    public void shouldReturnAtLeastOneMentorUsingDiffStringFromPullRequestURLForOwner() {
        PullReview pullReview = new PullReview(ANOTHER_TEST_REPOSITORY_ID, TEST_REPOSITORY_ID,
                TEST_PULL_REVIEW_NAME, TEST_PULL_REVIEW_DESCRIPTION, UserTest.createTestInstance(),
                TEST_PULL_REVIEW_CREATED_ON, TEST_PULL_REVIEW_UPDATED_ON, TEST_BRANCH_NAME);
        String someDiffString =
                "diff -r 6e9df89ba172268db2ff815caee67cc1c70298b1 -r 218c7ad8deb066b652970cb69bd8256cd7827f44 new_file\n" +
                        "--- /dev/null\n" +
                        "+++ b/new_file\n" +
                        "@@ -0,0 +1,1 @@\n" +
                        "+some new code";

        Set<Mentor> actualMentor = pullReview.evaluateMentorsFrom(someDiffString);

        assertThat(actualMentor.size(), is(not(equalTo(ZERO))));
    }
}
