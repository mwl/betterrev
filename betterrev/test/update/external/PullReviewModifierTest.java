package update.external;

import models.*;
import org.junit.Before;
import org.junit.Test;

import static models.PullReviewEventType.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PullReviewModifierTest extends AbstractPersistenceIntegrationTest {

    public static final String TEST_LINK_TO_EXTERNAL_INFO = "linkToExternalInfo";

    @Before
    public void setUp() {
        for (PullReview pullReview : PullReview.find.all()) {
            pullReview.delete();
        }
    }

    @Test
    public void rejectedPullReviewHasLinkToExternalInfo() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.PENDING_APPROVAL);

        pullReview = PullReviewModifier.rejectPullReview(pullReview.repositoryId, pullReview.pullRequestId,
                                                         TEST_LINK_TO_EXTERNAL_INFO);

        String linkToExternalInfo = null;
        for (PullReviewEvent pullReviewEvent : pullReview.pullReviewEvents) {
            if (pullReviewEvent.pullReviewEventType != null &&
                    pullReviewEvent.pullReviewEventType.equals(REJECTED)) {
                linkToExternalInfo = pullReviewEvent.linkToExternalInfo;
                break;
            }
        }
        assertThat("Link to External Info is not set correctly", linkToExternalInfo, is(TEST_LINK_TO_EXTERNAL_INFO));
    }


    @Test
    public void rejectedPullReviewContainsAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.PENDING_APPROVAL);

        pullReview = PullReviewModifier.rejectPullReview(pullReview.repositoryId, pullReview.pullRequestId,
                                                         TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find Rejected PullReviewEventType",
                   pullReview.hasPullReviewEventWith(REJECTED));
    }


    @Test
    public void mergedPullReviewHasAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.ACCEPTED);

        pullReview = PullReviewModifier.mergePullReview(pullReview.repositoryId, pullReview.pullRequestId,
                                                        TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find Merged PullReviewEventType",
                   pullReview.hasPullReviewEventWith(MERGED));
    }


    @Test
    public void mentorNotifiedPullReviewHasAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.OPEN);

        pullReview = PullReviewModifier.notifyMentor(pullReview.repositoryId, pullReview.pullRequestId,
                                                     TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find PullReviewEventType",
                   pullReview.hasPullReviewEventWith(MENTOR_NOTIFIED));
    }

}
