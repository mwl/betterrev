# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table interest (
  id                        bigint auto_increment not null,
  path                      varchar(255),
  project                   varchar(255),
  constraint pk_interest primary key (id))
;

create table mentor (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  email                     varchar(255),
  mentor_type               varchar(10),
  created_date              timestamp,
  constraint ck_mentor_mentor_type check (mentor_type in ('INDIVIDUAL','PROJECT','LIST')),
  constraint pk_mentor primary key (id))
;

create table pull_review (
  id                        bigint auto_increment not null,
  repository_id             varchar(255) not null,
  pull_request_id           varchar(255) not null,
  name                      varchar(255) not null,
  description               varchar(255),
  state                     varchar(16) not null,
  requester_id              bigint,
  created_on                timestamp not null,
  updated_on                timestamp not null,
  branch_name               varchar(255) not null,
  constraint ck_pull_review_state check (state in ('NULL','OPEN','PENDING_APPROVAL','ACCEPTED','CLOSED','COMMITTED')),
  constraint pk_pull_review primary key (id))
;

create table pull_review_event (
  id                        bigint auto_increment not null,
  pull_review_id            bigint,
  pull_review_event_type    varchar(27),
  link_to_external_info     varchar(255),
  created_date              timestamp,
  constraint ck_pull_review_event_pull_review_event_type check (pull_review_event_type in ('PULL_REVIEW_GENERATED','PULL_REVIEW_MODIFIED','MENTOR_NOTIFICATION_REQUEST','MENTOR_NOTIFIED','WEBREV_GENERATED','MERGED','APPROVED','REJECTED','TERMINATED')),
  constraint pk_pull_review_event primary key (id))
;

create table tag (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  constraint pk_tag primary key (id))
;

create table betterrev_user (
  id                        bigint auto_increment not null,
  bitbucket_user_name       varchar(255),
  openjdk_username          varchar(255),
  name                      varchar(255),
  oca_status                varchar(10),
  created_date              timestamp,
  constraint ck_betterrev_user_oca_status check (oca_status in ('SIGNED','NOT_SIGNED','UNKNOWN')),
  constraint pk_betterrev_user primary key (id))
;


create table mentor_interest (
  mentor_id                      bigint not null,
  interest_id                    bigint not null,
  constraint pk_mentor_interest primary key (mentor_id, interest_id))
;

create table pull_review_tag (
  pull_review_id                 bigint not null,
  tag_id                         bigint not null,
  constraint pk_pull_review_tag primary key (pull_review_id, tag_id))
;

create table pull_review_mentor (
  pull_review_id                 bigint not null,
  mentor_id                      bigint not null,
  constraint pk_pull_review_mentor primary key (pull_review_id, mentor_id))
;
alter table pull_review add constraint fk_pull_review_requester_1 foreign key (requester_id) references betterrev_user (id) on delete restrict on update restrict;
create index ix_pull_review_requester_1 on pull_review (requester_id);
alter table pull_review_event add constraint fk_pull_review_event_pullRevie_2 foreign key (pull_review_id) references pull_review (id) on delete restrict on update restrict;
create index ix_pull_review_event_pullRevie_2 on pull_review_event (pull_review_id);



alter table mentor_interest add constraint fk_mentor_interest_mentor_01 foreign key (mentor_id) references mentor (id) on delete restrict on update restrict;

alter table mentor_interest add constraint fk_mentor_interest_interest_02 foreign key (interest_id) references interest (id) on delete restrict on update restrict;

alter table pull_review_tag add constraint fk_pull_review_tag_pull_revie_01 foreign key (pull_review_id) references pull_review (id) on delete restrict on update restrict;

alter table pull_review_tag add constraint fk_pull_review_tag_tag_02 foreign key (tag_id) references tag (id) on delete restrict on update restrict;

alter table pull_review_mentor add constraint fk_pull_review_mentor_pull_re_01 foreign key (pull_review_id) references pull_review (id) on delete restrict on update restrict;

alter table pull_review_mentor add constraint fk_pull_review_mentor_mentor_02 foreign key (mentor_id) references mentor (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists interest;

drop table if exists mentor;

drop table if exists mentor_interest;

drop table if exists pull_review;

drop table if exists pull_review_tag;

drop table if exists pull_review_mentor;

drop table if exists pull_review_event;

drop table if exists tag;

drop table if exists betterrev_user;

SET REFERENTIAL_INTEGRITY TRUE;

